Title: Contributing
URL: contributing.html
save_as: contributing.html

We try to make it as easy as possible for you to contribute to the Trustable Software project with:

- ideas, suggestions and issues via IRC ([#trustable on freenode](http://webchat.freenode.net?randomnick=1&channels=trustable&uio=d4)) or [mailing list](https://lists.trustable.io/cgi-bin/mailman/listinfo/trustable-software)
- improvements to the website as merge requests or issues via its [gitlab repo](https://gitlab.com/trustable/trustable.gitlab.io)
- improvements to the wiki as merge requests or issues via its [gitlab page](https://gitlab.com/trustable/overview/wikis/home) 

General contributions to the website and wiki are licensed [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/) (public domain) by default. Contributions to software projects are covered by the licence(s) applicable to each project's own governance and/or repo(s).

