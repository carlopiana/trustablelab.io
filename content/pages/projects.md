Title: Relevant Projects
URL: projects.html
save_as: projects.html

There are many projects working to improve the trustability of software. This
is an informal list of some that we think are interesting.

- [ASAN](https://github.com/google/sanitizers/wiki/AddressSanitizer)
- [Bazel](https://bazel.build)
- [BuildStream](https://wiki.gnome.org/Projects/BuildStream)
- [CERT C](https://www.securecoding.cert.org/confluence/display/seccode/SEI+CERT+Coding+Standards)
- [CHERI](http://www.cl.cam.ac.uk/research/security/ctsrd/cheri/) and [CheriBSD](https://github.com/CTSRD-CHERI/cheribsd) 
- [Civil Infrastructure Platform](https://www.cip-project.org)
- [Core Infrastructure Initiative](https://www.coreinfrastructure.org)
- [DeepSpec](http://deepspec.org)
- [Doorstop](https://doorstop.readthedocs.io/en/latest/)
- [FOSSology](https://www.fossology.org)
- [Franca](https://github.com/franca/franca)
- [Jailhouse](https://github.com/siemens/jailhouse)
- [MISRA](https://www.misra.org.uk)
- [Muen](https://muen.codelabs.ch)
- [MSAN](http://clang.llvm.org/docs/MemorySanitizer.html)
- [NixOS](https://nixos.org)
- [OpenChain](https://openchainproject.org)
- [Open Control](http://open-control.org)
- [OpenSAMM](http://www.opensamm.org)
- [Openwall](https://www.openwall.com)
- [OSADL](http://www.osadl.org)
- [OSS-Fuzz](https://github.com/google/oss-fuzz)
- [PSAS](http://psas.scripts.mit.edu)
- [Quartermaster](http://qmstr.org)
- [Reproducible Builds](https://reproducible-builds.org)
- [REUSE Initiative](https://reuse.software/)
- [Rigorous Engineering of Mainstream Systems (REMS)](http://www.cl.cam.ac.uk/~pes20/rems/)
- [Rust](https://www.rust-lang.org/en-US/)
- [Safety-Critical Systems Club](https://scsc.org.uk)
- [SPARK](https://github.com/AdaCore/spark2014)
- [SEL4](https://sel4.systems)
- [SPDX](https://spdx.org)
- [The Software Commandments](https://github.com/devcurmudgeon/software_commandments)
- [The System Safety List](http://systemsafetylist.org) (signup [page](https://lists.techfak.uni-bielefeld.de/mailman/listinfo/systemsafety))
- [UBSAN](http://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html)

We're happy to add projects to the list. In future we may formalize criteria
for inclusion.

