Title: Trustable
URL:
save_as: index.html

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/494/badge)](https://bestpractices.coreinfrastructure.org/projects/494)

The Trustable Software project aims to improve the quality of enginering for
software which most people would consider *important*, for ourselves and for
our wider communities. Our scope includes any software which satisfies some or
all of the following criteria:

- may cause  harm to people either directly or indirectly
- may cause financial harm or damage productivity
- may cause harm to the environment, either directly or as a side-effect
- if hacked, could be exploited to cause any of the above

We propose that software can be considered trustable if

- we know where it comes from
- we know how to build it
- we can reproduce it
- we know what it does
- it does what it is supposed to do
- we can update it and be confident it will not break or regress
- we have some confidence that it won't harm our communities and our 
children

Our community is small, relatively well informed, skeptical, and honest about
the problems inherent in complex systems software. We're particularly
interested to involve experienced engineers from a wide range of industries
and organisations.

Based on our discussions and exploratory work so far, we focus on the
following factors:

- functionality
- provenance
- reproducibility
- reliability
- safety
- security
- compliance

Broadly we are working on ways to gather evidence which can be independently
assessed to consider whether or not a given body of work is trustable.

If you would like to help, please subscribe to the [mailing list](https://lists.trustable.io/cgi-bin/mailman/listinfo/trustable-software) and join the discussion there.
