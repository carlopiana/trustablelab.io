Title: Reading List
URL: reading.html
save_as: reading.html

The following lists include content which we hope is of interest to
readers working to improve the quality of trustable software. We're happy to
consider recommendations for additional reading.

<b>Note:</b> even the smartest authors are sometimes wrong, so buyer/reader beware.

### Software Concepts, Processes and Methods

[Making Software: What Really Works, and Why We Believe It](http://shop.oreilly.com/product/9780596808303.do) is a compilation of
articles about various software practices, mostly supported by actual
evidence.

[The Leprechauns of Software Engineering](https://leanpub.com/leprechauns)
debunks some widely held beliefs including 'the claimed 10x variation in productivity between developers; the "software crisis"; the cost-of-change curve; the "cone of uncertainty"'.

[SDL 3.0](https://www.amazon.co.uk/SDLC-3-0-Understanding-Generation-Engineering/dp/0986519405) attempts to map a landscape for Agile in context with more formal methods.

[Facts and Fallacies of Software Engineering](https://www.amazon.co.uk/Facts-Fallacies-Software-Engineering-Development/dp/0321117425) (and pretty much anything by Robert L. Glass).

Although Fred Brooks' [The Mythical Man-Month: Essays on Software Engineering](https://www.amazon.co.uk/Mythical-Man-month-Essays-Software-Engineering/dp/0201835959) was originally published in 1975, it's still required reading. The key ideas are summarised on its [wiki page](https://en.wikipedia.org/wiki/The_Mythical_Man-Month).

### Philosophical and Psychological factors

Nicholas Taleb's [Antifragile: Things that Gain from Disorder](https://www.amazon.co.uk/Antifragile-Things-that-Gain-Disorder/dp/0141038225) and [The Black Swan](https://www.amazon.co.uk/Black-Swan-Impact-Highly-Improbable/dp/0141034599) are not about software, but both highlight the dangers in complexity and how easy it is for people to believe the wrong things.

[Thinking, Fast and Slow](https://www.amazon.co.uk/Thinking-Fast-Slow-Daniel-Kahneman/dp/0141033576) describes in detail a wide range of ways in which our brains draw the wrong
conclusions.

### Papers, Articles and Lectures

Ken Thompson's [Reflections on Trusting Trust](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf) highlights just how easy it is to backdoor software.

BCS report on[Safety-Critical vs Security-Critical Software](http://www.bcs.org/upload/pdf/safety-v-security-report.pdf)

David Parnas' [A Rational Design Process: How and Why to Fake it](https://www.researchgate.net/publication/260649064_A_Rational_Design_Process_How_and_Why_to_Fake_it)

Salzer and Schroeder's [The Protection of Information in Computer Systems](http://web.mit.edu/Saltzer/www/publications/protection/) or Mathew Squair's [summarised principles](https://criticaluncertainties.com/reference/saltzer-and-schroeders-principles/)
