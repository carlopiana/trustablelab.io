#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'trustable'
SITENAME = u'trustable software engineering'
SITEURL = '.'

PATH = 'content'

THEME = './theme'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('white paper', 'static/towards-trustable-software-white-paper.pdf'),
         ('wiki', 'https://gitlab.com/trustable/overview/wikis/home'),
         ('mailing list', 'https://lists.trustable.io/cgi-bin/mailman/listinfo/trustable-software'),
         ('irc logs', 'https://irclogs.baserock.org/trustable/latest.log.html'),
         ('#trustable on freenode', 'http://webchat.freenode.net?randomnick=1&channels=trustable&uio=d4'),
         ('code', 'https://gitlab.com/trustable'),)


DEFAULT_PAGINATION = False
DISPLAY_PAGES_ON_MENU = True

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

STATIC_PATHS = ['static', 'extra/CNAME']
EXTRA_PATH_METADATA = {'extra/CNAME': {'path': 'CNAME'},}
